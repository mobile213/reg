import 'package:flutter/material.dart';

class Body extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    final urlImage = 'assets/images/buu.png';
    final urlImage1 = '../assets/images/pic.png';
    Size size = MediaQuery.of(context).size;
    return SingleChildScrollView(
      
        child: Column(
          children: <Widget>[
            Container(
              height: size.height * 0.2,
              child: Stack(
                children: <Widget>[
                  Container(
                    height: size.height * 0.2 - 27,
                    decoration: BoxDecoration(
                        color: Colors.amber,
                        borderRadius: BorderRadius.only(
                          bottomLeft: Radius.circular(36),
                          bottomRight: Radius.circular(36),
                        ),
                        boxShadow: [
                          BoxShadow(
                            offset: Offset(0,10),
                            blurRadius:  50,
                            color: Color.fromARGB(255, 52, 51, 51),
                          )
                        ]
                    ),
                  ),
                  Positioned(
                    left: 40,
                    top: 7,
                    child: Image.asset(
                      urlImage,
                      width: 90,
                      height: 90,
                    ),
                  ),
                  Positioned(
                      left: 150,
                      top: 7,
                      child: Text('มหาวิทยาลัยบูรพา',
                          style: TextStyle(
                              fontSize: 28.0,
                              fontWeight: FontWeight.bold,
                              color: Color.fromARGB(255, 116, 116, 116)))
                  ),
                  Positioned(
                      left: 150,
                      top: 50,
                      child: Text('BURAPHA UNIVERSITY',
                          style: TextStyle(
                              fontSize: 20.0,
                              fontWeight: FontWeight.bold,
                              color: Color.fromARGB(255, 255, 255, 255)))),

                ],
              ),
            ),
            Container(
                child: Stack(
                    children: <Widget>[
                      Container(
                        height: 800,
                        decoration: BoxDecoration(
                            color: Color.fromARGB(255, 252, 252, 252),
                            borderRadius: BorderRadius.only(
                              topLeft: Radius.circular(36),
                              topRight: Radius.circular(36),
                            ),
                            boxShadow: [
                              BoxShadow(
                                offset: Offset(0,10),
                                blurRadius:  50,
                                color: Color.fromARGB(255, 200, 198, 198),
                              )
                            ]
                        ),
                      ),

                      // Positioned(
                      //     left: 37,
                      //     top: 20,
                      //     child: Container(
                      //         child: ElevatedButton(
                      //           style: ElevatedButton.styleFrom(
                      //             minimumSize: Size(30, 30),
                      //             textStyle: TextStyle(fontSize: 10),
                      //           ),
                      //           child: Text('เกี่ยวกับทะเบียนฯ') ,
                      //           onPressed: () {},
                      //         )
                      //     )
                      // ),
                      // Positioned(
                      //     left: 150,
                      //     top: 20,
                      //     child: Container(
                      //         child: ElevatedButton(
                      //           style: ElevatedButton.styleFrom(
                      //             minimumSize: Size(30, 30),
                      //             textStyle: TextStyle(fontSize: 10),
                      //           ),
                      //           child: Text('แนะนำการลงทะเบียน') ,
                      //           onPressed: () {},
                      //         )
                      //     )
                      // ),
                      // Positioned(
                      //   left: 280,
                      //   top: 16,
                      //   child: Image.asset(
                      //     urlImage2,
                      //     width: 30,
                      //     height: 30,
                      //   ),
                      // ),

                      Positioned(
                          left: 37,
                          top: 50,
                          child: Text('63160210 : นายพีชญุตม์ ธนะประสพ : คณะวิทยาการสารสนเทศ',
                            style:const TextStyle(
                              color: Color.fromARGB(255, 0, 0, 0),
                              fontSize: 13,
                              fontWeight: FontWeight.w500,
                            ),)
                      ),
                      Positioned(
                          left: 37,
                          top: 70,
                          child: Text('หลักสูตร: 2115020: วท.บ. (วิทยาการคอมพิวเตอร์) ปรับปรุง 59 - ป.ตรี 4 ปี',
                            style:const TextStyle(
                              color: Color.fromARGB(255, 14, 2, 75),
                              fontSize: 10,
                              fontWeight: FontWeight.w600,
                            ),)
                      ),
                      Positioned(
                          left: 37,
                          top: 85,
                          child: Text('วิชาโท: 0: -',
                            style:const TextStyle(
                              color: Color.fromARGB(255, 14, 2, 75),
                              fontSize: 10,
                              fontWeight: FontWeight.w600,
                            ),)
                      ),

                      Positioned(
                          left: 37,
                          top: 100,
                          child: Text('สถานภาพ: กำลังศึกษา',
                            style:const TextStyle(
                              color: Color.fromARGB(255, 14, 2, 75),
                              fontSize: 10,
                              fontWeight: FontWeight.w600,
                            ),)
                      ),

                      Positioned(
                          left: 37,
                          top: 115,
                          child: Text('อ. ที่ปรึกษา: ผู้ช่วยศาสตราจารย์ ดร.โกเมศ อัมพวัน,อาจารย์ภูสิต กุลเกษม',
                            style:const TextStyle(
                              color: Color.fromARGB(255, 14, 2, 75),
                              fontSize: 10,
                              fontWeight: FontWeight.w600,
                            ),)
                      ),

                      Positioned(
                          left: 37,
                          top: 130,
                          child: Text('______________________________________________________________________',
                            style:const TextStyle(
                              color: Color.fromARGB(255, 255, 195, 57),
                              fontSize: 10,
                              fontWeight: FontWeight.w600,
                            ),)
                      ),

                      Positioned(
                          left: 37,
                          top: 150,
                          child: Text('ประกาศเรื่อง',
                            style:const TextStyle(
                              color: Color.fromARGB(255, 14, 2, 75),
                              fontSize: 14,
                              fontWeight: FontWeight.w600,
                            ),)
                      ),

                      Positioned(
                          left: 37,
                          top: 170,
                          child: Text('1.แบบประเมินความคิดเห็นของนักเรียนและนิสิตต่อการให้บริการของสำนักงานอธิการบดี',
                            style:const TextStyle(
                              color: Color.fromARGB(255, 14, 2, 75),
                              fontSize: 10,
                              fontWeight: FontWeight.w600,
                            ),)
                      ),
                      Positioned(
                          left: 37,
                          top: 185,
                          child: Text('(ด่วนที่สุด)',
                            style:const TextStyle(
                              color: Color.fromARGB(255, 255, 0, 0),
                              fontSize: 10,
                              fontWeight: FontWeight.w600,
                            ),)
                      ),
                      Positioned(
                          left: 37,
                          top: 200,
                          child: Text('ขอเชิญนิสิตร่วมทำแบบประเมินความคิดเห็นของนิสิต',
                            style:const TextStyle(
                              color: Color.fromARGB(255, 255, 0, 0),
                              fontSize: 13,
                              fontWeight: FontWeight.w400,
                            ),)
                      ),
                      Positioned(
                          left: 37,
                          top: 215,
                          child: Text('ต่อการให้บริการของสำนักงานอธิการบดี',
                            style:const TextStyle(
                              color: Color.fromARGB(255, 255, 0, 0),
                              fontSize: 13,
                              fontWeight: FontWeight.w400,
                            ),)
                      ),
                      Positioned(
                          left: 37,
                          top: 235,
                          child: Text('https://bit.ly/3cyvuuf',
                            style:const TextStyle(
                              color: Color.fromARGB(255, 0, 128, 255),
                              fontSize: 12,
                              fontWeight: FontWeight.w400,
                            ),)
                      ),

                      Positioned(
                          left: 37,
                          top: 260,
                          child: Text('2.การทำบัตรนิสิตกับธนาคารกรุงไทย',
                            style:const TextStyle(
                              color: Color.fromARGB(255, 14, 2, 75),
                              fontSize: 10,
                              fontWeight: FontWeight.w600,
                            ),)
                      ),
                      Positioned(
                          left: 37,
                          top: 275,
                          child: Text('กรณีบัตรหายเสียค่าใช้จ่ายในการทำ 100 บาท',
                            style:const TextStyle(
                              color: Color.fromARGB(255, 255, 0, 0),
                              fontSize: 12,
                              fontWeight: FontWeight.w400,
                            ),)
                      ),
                      Positioned(
                          left: 37,
                          top: 290,
                          child: Text('สำหรับนิสิตรหัส 65 วิทยาเขตบางแสนที่เข้าภาคเรียนที่ 1 ที่ยังไม่รับบัตร',
                            style:const TextStyle(
                              color: Color.fromARGB(255, 255, 0, 0),
                              fontSize: 12,
                              fontWeight: FontWeight.w400,
                            ),)
                      ),
                      Positioned(
                          left: 37,
                          top: 305,
                          child: Text('ให้ติดต่อรับบัตรนิสิตที่ธนาคารกรุงไทย สาขา ม.บูรพา ส่วนนิสิต',
                            style:const TextStyle(
                              color: Color.fromARGB(255, 255, 0, 0),
                              fontSize: 12,
                              fontWeight: FontWeight.w400,
                            ),)
                      ),
                      Positioned(
                          left: 37,
                          top: 320,
                          child: Text('ที่เข้าภาคเรียนที่ 2/2565 รอกำหนดการอีกครั้ง',
                            style:const TextStyle(
                              color: Color.fromARGB(255, 255, 0, 0),
                              fontSize: 12,
                              fontWeight: FontWeight.w400,
                            ),)
                      ),
                      Positioned(
                        left: 37,
                        top: 345,
                        child: Image.asset(
                          urlImage1,
                          width: 300,
                          height: 300,
                        ),
                      ),

                      Positioned(
                          left: 37,
                          top: 660,
                          child: Text('______________________________________________________________________',
                            style:const TextStyle(
                              color: Color.fromARGB(255, 255, 195, 57),
                              fontSize: 10,
                              fontWeight: FontWeight.w600,
                            ),)
                      ),



                    ]
                )
            )

          ],
        )
    );
  }
}
