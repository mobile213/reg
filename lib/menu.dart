import 'dart:html';

import 'package:flutter/material.dart';
import 'body.dart';
import 'sidebar.dart';

class Menu extends StatelessWidget {
  const Menu({super.key});


  @override
  Widget build(BuildContext context) {
    return Scaffold(
      floatingActionButton: FloatingActionButton(
        onPressed: ()=>{},
        child: Icon(Icons.arrow_upward),
        backgroundColor: Colors.amberAccent,
      ),
      drawer: SideBar(),
      appBar: buildAppBar(),
      body: Body(),
    );
  }

  AppBar buildAppBar() {
    return AppBar(
      // Colors: Colors.green,
      elevation: 0,
    );

  }

}


