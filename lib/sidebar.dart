import 'package:flutter/material.dart';

class SideBar extends StatelessWidget {

  @override
  Widget build(BuildContext context) {
    return Drawer(
      child: ListView(
        children: [
          Container(
            height: 50,
            decoration: BoxDecoration(
              color: Color.fromARGB(255, 255, 254, 252),

            ),
          ),
          ListTile(
            leading: Icon(Icons.menu),
            title: Text('เมนูหลัก'),
            onTap: () => null,
          ),
          // ListTile(
          //   leading: Icon(Icons.login),
          //   title: Text('เข้าสู่ระบบ'),
          //   onTap: () => null,
          // ),
          ListTile(
            leading: Icon(Icons.menu_book_sharp),
            title: Text('ลงทะเบียน'),
            onTap: () => null,
          ),
          ListTile(
            leading: Icon(Icons.table_chart_rounded),
            title: Text('ผลการลงทะเบียน'),
            onTap: () => null,
          ),
          ListTile(
            leading: Icon(Icons.checklist_rtl),
            title: Text('ผลอนุมัติเพิ่ม-ลด'),
            onTap: () => null,
          ),
          ListTile(
            leading: Icon(Icons.calendar_month),
            title: Text('ตารางเรียน/สอบ'),
            onTap: () => null,
          ),
          ListTile(
            leading: Icon(Icons.history_edu),
            title: Text('ประวัตินิสิต'),
            onTap: () => null,
          ),
          ListTile(
            leading: Icon(Icons.account_balance_wallet_outlined),
            title: Text('ภาระค่าใช้จ่ายทุน'),
            onTap: () => null,
          ),
          ListTile(
            leading: Icon(Icons.people_alt),
            title: Text('ผลการศึกษา'),
            onTap: () => null,
          ),
          ListTile(
            leading: Icon(Icons.school),
            title: Text('ตรวจสอบจบ'),
            onTap: () => null,
          ),
          ListTile(
            leading: Icon(Icons.newspaper),
            title: Text('ยื่นคำร้อง'),
            onTap: () => null,
          ),
          ListTile(
            leading: Icon(Icons.question_answer_outlined),
            title: Text('เสนอความคิดเห็น'),
            onTap: () => null,
          ),
          ListTile(
            leading: Icon(Icons.receipt_long_rounded),
            title: Text('รายชื่อนิสิต'),
            onTap: () => null,
          ),
          ListTile(
            leading: Icon(Icons.description_outlined),
            title: Text('ประวัติการเข้าใช้ระบบ'),
            onTap: () => null,
          ),
          ListTile(
            leading: Icon(Icons.attach_money,
                color: Colors.red),
            title: Text('ตรวจหนี้สิน(นิสิตที่ยังไม่จบ)',
                style: TextStyle(color: Colors.red)),
            onTap: () => null,
          ),

          ListTile(
             leading: Icon(Icons.login),
             title: Text('ออกจากระบบ'),
             onTap: () => null,
    ),
        ],
      ),
    );
  }
}